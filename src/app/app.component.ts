import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public dropdownList = [
    {
      id: 1,
      label: 'Test1',
    },
    {
      id: 2,
      label: 'Test2',
    },
    {
      id: 3,
      label: 'Test3',
    },
    {
      id: 4,
      label: 'Test4',
    },
  ];

  change($event: any) {
    console.log($event);
  }
}
