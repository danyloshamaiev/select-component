import {
  ChangeDetectorRef,
  Component,
  Input,
  NgZone,
  OnInit,
  Output,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import {filter, takeUntil} from "rxjs/operators";
import Popper from "popper.js";
import {EventEmitter} from "@angular/core";
import {fromEvent} from "rxjs";

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {
  @Input() labelKey = 'label';

  @Input() idKey = 'id';

  @Input() options: any[] = [];

  @Input() model: any;

  @Input() multiple = false;

  @Output() selectChange = new EventEmitter();

  @Output() closed = new EventEmitter();

  private originalOptions: any;

  private view: any;

  private popperRef: any;

  get isOpen() {
    return !!this.popperRef;
  }

  get label() {
    const _default = 'Select...';
    //return this.model ? this.model[this.labelKey] : 'Select...';
    if (this.model) {
      if (this.multiple) {
        const items = [];
        for (const element of this.model) {
          if (element)
            items.push(element[this.labelKey]);
        }
        return items.join(', ');
      } else {
        return this.model[this.labelKey];
      }
    } else {
      return _default;
    }
  }

  constructor(private vcr: ViewContainerRef, private zone: NgZone, private cdr: ChangeDetectorRef) {}

  ngOnInit() {
    this.originalOptions = [...this.options];

    if (this.model !== undefined) {
      this.model = this.options.find(
        currentOption => currentOption[this.idKey] === this.model
      );
    }
  }

  isActive(option: any) {
    if (!this.model) {
      return false;
    }

    if (this.multiple) {
      console.log(this.model);
      return this.model.findIndex(
        (element: any) => {
          if (element)
            return option[this.idKey] === element[this.idKey]
          else
            return false;
        }) !== -1;
    } else {
      return option[this.idKey] === this.model[this.idKey];
    }
  }

  open(dropdownTpl: TemplateRef<any>, origin: HTMLElement) {
    if (this.isOpen) {
      this.close();
      return;
    }

    this.view = this.vcr.createEmbeddedView(dropdownTpl);
    const dropdown = this.view.rootNodes[0];

    document.body.appendChild(dropdown);

    this.zone.runOutsideAngular(() => {
      this.popperRef = new Popper(origin, dropdown, {
        removeOnDestroy: true
      });
    });

    this.handleClickOutside();
  }

  close() {
    this.closed.emit();
    this.popperRef.destroy();
    this.view.destroy();
    this.view = null;
    this.popperRef = null;
  }

  select(option: any) {
    if (this.multiple) {
      if (!this.model) {
        this.model = [option];
      }
      else {
        const index = this.model.findIndex(
          (element: any) => {
            if (element)
              return option[this.idKey] === element[this.idKey]
            else
              return false;
          });

        if (index !== -1) {
          delete this.model[index];
          return;
        }

        this.model = [...this.model, option];
      }
    } else {
      this.model = option;
    }
    this.selectChange.emit(option[this.idKey]);
  }

  private handleClickOutside() {
    fromEvent(document, 'click')
      .pipe(
        filter(({ target }) => {
          const origin = this.popperRef.reference as HTMLElement;
          return !origin.contains(target as HTMLElement);
        }),
        takeUntil(this.closed)
      )
      .subscribe(() => {
        this.close();
        this.cdr.detectChanges();
      });
  }

}
